using UnityEngine;
using Uduino;

public class RightHandMove : MonoBehaviour
{

    Vector3 position;
    Vector3 rotation;
    public Vector3 rotationOffset;
    public float speedFactor = 15.0f;
    public string imuName = "r2"; // You should ignore this if there is one IMU.
    bool firstUsefulRead = true;
    int firstUsefulReadCounter = 0;

    //static float start_x = -0.077f;
    //static float start_y = -0.099f;
    //static float start_z = 0.027f;
    //static float start_w = 0.992f;
    static float start_x = -0.186f;
    static float start_y = 0.388f;
    static float start_z = -0.290f;
    static float start_w = 0.855f;

    static float ref_x = 0.4f;
    static float ref_y = 0.6f;
    static float ref_z = -0.6f;
    static float ref_w = -0.3f;

    static float start_yaw = -8.351f;
    static float start_pitch = 50.428f;
    static float start_roll = -33.58f;

    static float ref_yaw = 0.4f;
    static float ref_pitch = 0.6f;
    static float ref_roll = -0.6f;

    void Start()
    {
        //  UduinoManager.Instance.OnDataReceived += ReadIMU;
        //  Note that here, we don't use the delegate but the Events, assigned in the Inpsector Panel
    }

    void Update() { }

    public void ReadIMU(string data, UduinoDevice device)
    {

        string[] values = data.Split('/');

        if (values.Length == 4 && values[0] == imuName) // Rotation of the first one 
        {

            if (firstUsefulRead)
            {
                firstUsefulReadCounter++;
                if (firstUsefulReadCounter == 100)
                {
                    ref_yaw = float.Parse(values[1], System.Globalization.CultureInfo.InvariantCulture);
                    ref_pitch = float.Parse(values[2], System.Globalization.CultureInfo.InvariantCulture);
                    ref_roll = float.Parse(values[3], System.Globalization.CultureInfo.InvariantCulture);
                    firstUsefulRead = false;
                }
            }
            float yaw = float.Parse(values[1], System.Globalization.CultureInfo.InvariantCulture);
            float pitch = float.Parse(values[2], System.Globalization.CultureInfo.InvariantCulture);
            pitch *= -1;
            float roll = float.Parse(values[3], System.Globalization.CultureInfo.InvariantCulture);
            roll *= -1;
            //Debug.LogWarning(yaw + "\t" + pitch + "\t" + roll);

            //Quaternion q = new Quaternion((x - ref_x),(y - ref_y),(z - ref_z),(w - ref_w));


            //Vector3 currentEulerAngles = q.eulerAngles;
            //currentEulerAngles[1] *= -1;
            //Vector3 rotationVector = new Vector3(yaw, pitch, roll);
            //Quaternion q = new Quaternion(start_x + (x-ref_x), start_y + (y-ref_y), start_z + (z-ref_z), start_w + (w-ref_w));
            //Quaternion q = Quaternion.Euler(start_yaw + (yaw - ref_yaw), start_pitch + (pitch - ref_pitch), start_roll + (roll - ref_roll));
            Quaternion q = Quaternion.Euler(start_yaw + (yaw - ref_yaw), start_pitch + (pitch - ref_pitch), start_roll + (ref_roll - ref_roll));
            //Debug.LogWarning(q);
            this.transform.localRotation = Quaternion.Lerp(this.transform.localRotation, q, Time.deltaTime * speedFactor);

            //this.transform.localRotation = q;
        }
        else if (values.Length != 5)
        {
            //Debug.LogWarning(data);
        }
        //this.transform.parent.transform.eulerAngles = rotationOffset;
        //Log.Debug("The new rotation is : " + transform.Find("EthanRightArm").eulerAngles);
    }

    //public void ReadIMUQuaternion(string data, UduinoDevice device)
    //{
    //    //Debug.Log(data);
    //    string[] values = data.Split('/');
    //    if (values.Length == 5 && values[0] == imuName) // Rotation of the first one 
    //    {
    //        if (firstUsefulRead)
    //        {
    //            firstUsefulReadCounter++;
    //            if (firstUsefulReadCounter == 100)
    //            {
    //                ref_w = float.Parse(values[1], System.Globalization.CultureInfo.InvariantCulture);
    //                ref_x = float.Parse(values[2], System.Globalization.CultureInfo.InvariantCulture);
    //                ref_y = float.Parse(values[3], System.Globalization.CultureInfo.InvariantCulture);
    //                ref_z = float.Parse(values[4], System.Globalization.CultureInfo.InvariantCulture);
    //                firstUsefulRead = false;
    //            }
    //        }
    //        float w = float.Parse(values[1], System.Globalization.CultureInfo.InvariantCulture);
    //        float x = float.Parse(values[2], System.Globalization.CultureInfo.InvariantCulture);
    //        float y = float.Parse(values[3], System.Globalization.CultureInfo.InvariantCulture);
    //        float z = float.Parse(values[4], System.Globalization.CultureInfo.InvariantCulture);
    //        //Debug.LogWarning(w + "\t" + x + "\t" + y + "\t" + z + "\t");

    //        //Quaternion q = new Quaternion((x - ref_x),(y - ref_y),(z - ref_z),(w - ref_w));

    //        Debug.LogWarning("x=" + x + "\t y=" + y + "\t z=" + z + "\t w=" + w);
    //        Debug.LogWarning("ref_x=" + ref_x + "\t ref_y=" + ref_y + "\t ref_z=" + ref_z + "\t ref_w=" + ref_w);
    //        Debug.LogWarning("kul_x=" + (x - ref_x) + "\t kul_y=" + (y - ref_y) + "\t kul_z=" + (z - ref_z) + "\t kul_w=" + (w - ref_w));
    //        Quaternion q = new Quaternion(start_x + (x-ref_x), start_y + (y-ref_y), start_z + (z-ref_z), start_w + (w-ref_w));

    //        //Vector3 currentEulerAngles = q.eulerAngles;
    //        //currentEulerAngles[1] *= -1;
    //        //q = Quaternion.Euler(currentEulerAngles);

    //        this.transform.localRotation = Quaternion.Lerp(this.transform.localRotation, q, Time.deltaTime * speedFactor);

    //        //this.transform.localRotation = q;
    //        Debug.LogWarning(q);
    //    }
    //    else if (values.Length != 5)
    //    {
    //        Debug.LogWarning(data);
    //    }
    //    //this.transform.parent.transform.eulerAngles = rotationOffset;
    //    //Log.Debug("The new rotation is : " + transform.Find("EthanRightArm").eulerAngles);
    //}
}
